# -*- coding: utf-8 -*-
"""
Created on Thu Oct 28 18:49:45 2021

    Sodoku solver 
        with
    sodoku generator
        by 
    Pedro Galdames
    
 El objetivo del programa es crear un solucionador de 
sodokus.
Pero como extra, se genera un sodoku aleatoriamente el cual
tambien se resulve.   
    
"""

 
import requests # se importa libreria para requests

response = requests.get("https://sugoku.herokuapp.com/board?difficulty=easy") # Se llama a un sodoku aleatorio
S1 = response.json()['board'] #se guarda en una variable el sodoku
S1_original = [[S1[x][y] for y in range(len(S1[0]))] for x in range(len(S1))]

S = [
        [7, 8, 0, 4, 0, 0, 1, 2, 0],     # se crea un sodoku de ejemplo
        [6, 0, 0, 0, 7, 5, 0, 0, 9],
        [0, 0, 0, 6, 0, 1, 0, 7, 8],
        [0, 0, 7, 0, 4, 0, 2, 6, 0],
        [0, 0, 1, 0, 5, 0, 9, 3, 0],
        [9, 0, 4, 0, 6, 0, 0, 0, 5],
        [0, 7, 0, 3, 0, 0, 0, 1, 2],
        [1, 2, 0, 0, 0, 7, 4, 0, 0],
        [0, 4, 9, 2, 0, 6, 0, 0, 7]
            ]

size=len(S)     #tamaño de la matrix

################# representacion grafica del sodoku S #################
print("Sodoku de ejemplo:")
print("\n\n+ ----------- + ----------- + ----------- +",end="")
for i in range(0,size):
    print("\n",end="\n|  ")
    for j in range(0,size):
        num_end = "  |  " if ((j+1)%3 == 0) else "   "
        print(S[i][j],end=num_end)
    
    if ((i+1)%3 == 0):
        print("\n\n+ ----------- + ----------- + ----------- +",end="")
        print("\n\n+ ----------- + ----------- + ----------- +",end="")

#######################################################################

################# funcion check () ####################################
########## el objetivo es tomar la posicion (x,y) y verificar si es posible###
######### poner el numero n es la fila, columna o cuadrado ################

def check(y,x,n,m): #se verificar si es posible poner 1 numero en una posicion i,j del sodoku
    
    
    for i in range(0,size): #se verifica fila
        if m[y][i] == n :
            return False
    for i in range (0,size): #se verifica columna
        if m[i][x] == n:
            return False
    start_col=(x//3)*3 
    start_row=(y//3)*3
    
    for i in range(0,3): #se verifica cuadrado adyacente
        for j in range(0,3):
            if m[start_row+1][start_col+j]==n:
                return False
    return True        
#######################################################################

##################### funcion solve()##################################
######## Esta funcion toma los espacios con un 0 y verificar ##########
######## es posible poner un numero en tal posicion, de ser posible####
######## asigna valor, de no serlo prueba el siguiente numero##########
def solve(m):
    
    for y in range(0,size):        #se toma cada posicion (x,y) de la matrix 
        for x in range(0,size):
            if m[y][x]==0:         #si esta es 0, se verifica si es posible el numero n
                for n in range(1,10):
                    if check(y,x,n,m): #si check= true, la posicion = n
                        m[y][x]=n
                        solve(m)
                        m[y][x]=0
                return       
    print("Solucion del sodoku:")    #se representa de forma grafica en consola la solucion            
    print("\n\n+ ----------- + ----------- + ----------- +",end="")
    for i in range(0,size):
        print("\n",end="\n|  ")
        for j in range(0,size):
            num_end = "  |  " if ((j+1)%3 == 0) else "   "
            print(m[i][j],end=num_end)
    
        if ((i+1)%3 == 0):
            print("\n\n+ ----------- + ----------- + ----------- +",end="")

########################################################################

solve(S) # se llama a la funcion para resolver sodoku S


############### En esta porcion de codigo se resuelve #################
#un sodoku aleatorio, este puede tomar mucho tiempo por ###############
# todas soluciones posibles, ademas de que puede llenar ###############
# la consola, por lo que si no tiene tiempo, sacar esta ###############
# parte del codigo
size=len(S1)
print("Sodoku generado aleatoriamente:")
print("\n\n+ ----------- + ----------- + ----------- +",end="")
for i in range(0,size):
    print("\n",end="\n|  ")
    for j in range(0,size):
        num_end = "  |  " if ((j+1)%3 == 0) else "   "
        print(S1[i][j],end=num_end)
    
    if ((i+1)%3 == 0):
        print("\n\n+ ----------- + ----------- + ----------- +",end="")
        print("\n\n+ ----------- + ----------- + ----------- +",end="")

solve(S1) # se resuelve sodoku generado aleatoriamente
#########################################################################
